import { useState } from 'react'
import reactLogo from './assets/react.svg'
import Componente1 from './Components/Componente1'
import Componente2 from './Components/Componente2'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
    <Componente1></Componente1>
    <Componente2></Componente2>
    </>
  )
}

export default App
